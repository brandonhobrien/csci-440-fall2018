/* Find those students for whom all of their friends are in different
grades from themselves. Return the students' names and grades. */

.read data.sql
select A.name, A.grade
from Highschooler as A
where A.grade not in (
  select B.grades
  from Highschooler as B, Friend as F
  where A.ID = F.ID1 and B.ID = F.ID2
);
