/* For each student A who likes a student B where the
two are not friends, find if they have a friend C in
common (who can introduce them!). For all such trios,
return the name and grade of A, B, and C. */

.read data.sql

select A.name, A.grade, B.name, B.grade, C.name, C.grades
from Highschooler as A, Highschooler as B, Highschooler as C, Friend as F, Friend as F2, Likes as L
where (A.ID = L.ID1 and B.ID = L.ID2) and B.ID not in
        (select ID2 from Friend where A.ID = L.ID1) and
        A.ID = F.ID1 and C.ID = F.ID2 and B.ID = F2.ID1 and C.ID = F2.ID2;
)
