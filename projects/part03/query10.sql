/* For every student who likes someone 2 or more grades younger than themselves, return that student's name and grade, and the name and grade of the student they like. */

.read data.sql

select distinct A1.name, A1.grade, A2.name, A2.grade
from (select S1.name as A1.name, S1.grade as A1.grade, S2.name as A2.name, S2.grade as A2.grade, S1.grade-S2.grade as gradingDifference
from Highschooler S1, Likes, Highschooler S2
where S1.ID = ID1 and S2.ID = ID2)
where gradingDifference>1;