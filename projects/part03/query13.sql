/* For every situation where student A likes student B,
but we have no information about whom B likes (that is,
B does not appear as an ID1 in the Likes table), return
A and B's names and grades. */

.read data.sql

select A.name, A.grade, B.name, B.grade
from Highschooler as A, Highschooler as B, Likes as L
where A.ID = L.ID1 and B.ID = B.ID2 and B.ID not in (select ID1 from Likes);
