/*Find the number of students who are either friends with Cassandra
or are friends of friends of Cassandra. Do not count Cassandra, even
though technically she is a friend of a friend.*/

.read data.sql

select count(ID2)
from Friend
where ID1 in (
  select ID2
  from Friend
  where ID1 in (
    select ID
    from Highschooler
    where Highschooler.name = 'Cassandra'))
and ID1 not in (
  select ID
  from Highschooler
  where Highschooler.name = 'Cassandra');
