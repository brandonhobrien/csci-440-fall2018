/* What is the average number of friends per student? (Your result should be just one number.) */

.read data.sql

select avg(x) from (select ID1, count(ID2) x from friend group by ID1);