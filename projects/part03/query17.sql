/* Find the name and grade of all students who are
liked by more than one other student. */

.read data.sql

select A.name, A.grade
from Highschooler as A, (select ID2, count(ID2) as num
                          from likes
                          group by ID2)
where num > 1 and ID2 = A.ID
