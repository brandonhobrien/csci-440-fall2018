/* Find names and grades of students who only have friends in the same grade. Return the result sorted by grade, then by name within each grade. */

read data.sql

select name, grade from Highschooler 
where ID not in (select ID1 from Highschooler A1, Friend, Highschooler A2 where A1.ID = Friend.ID1 and Friend.ID2 = A2.ID and A1.grade <> A2.grade)
order by grade, name;