/* For every pair of students who both like each other,
return the name and grade of both students. Include each
pair only once, with the two names in alphabetical order. */

.read data.sql

select A.name, A.grade, B.name, B.grades
from Highschooler as A, Highschooler as B, Likes as L, Likes as L2
where (A.ID = L.ID1 and B.ID = L.ID2) and (A.ID = L2.ID2 and B.ID = L2.ID1) and A.name < B.name;
