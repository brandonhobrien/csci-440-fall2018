/* Find the difference between the number of students in the school and the number of different first names. */

read data.sql

select (select count(ID) from Highschooler) - (select count(distinct name) from Highschooler);