/* Find the name and grade of the student(s) with the greatest number of friends. */

.read data.sql
select A.name, A.grade from Highschooler A, Friend X where
A.ID = X.ID1 group by X.ID1 having count(X.ID2) = (
select max(a.b) from 
(select count(ID2) as a from Friend group by ID1) as b);