# Team Members

* Brandon O'Brien
* Arjan Pokharel

# 3NF Normalization 
 The objective of this tutorial is to provide steps towards setting a database to 3rd Normal form. 
 As higher forms of normalization are needed, the process becomes more complex, so assuming we can 
 have a database in 2NF and have no transitive functional dependencies, we can work to converting 
 to the 3NF. The reader should find it easy to convert the data to 3NF after reading the tutorial. 
 The value of this conversion is to avoid data redundancy and anomalies. The advantage of 3NF is 
 that we can always backtrack to 2NF or 1NF if needed, since these are needed to get the data to 3NF. 

# Basic SQL
 The objective of this tutorial is to have a user understand the basic functionalities of the SQL 
 Language and communicate with a database. SQL commands are needed often to store, manipulate, and 
 retrieve database data, so without a proper tutorial, it could get complicated fast. The reader 
 should easily be able to work their way around communication with a database after reading our 
 tutorial, including getting started with SQL. The advantage of such a tutorial provides an easy 
 entry to the SQL Language and the understanding of Databases in general. 

# ER Converting to Relational Mapping
 The objective of this tutorial is to convert ER diagrams of all relationship and cardinality types 
 to Relational models. In a RDBMS, ER diagrams are not understood by the system, however Relational 
 maps are, so the conversion is valuable to the user. The reader will find the conversion simple after 
 reading the tutorial, and will hopefully be able to implement in a RDBMS.

